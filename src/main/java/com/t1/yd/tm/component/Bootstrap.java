package com.t1.yd.tm.component;

import com.t1.yd.tm.api.ICommandController;
import com.t1.yd.tm.api.ICommandRepository;
import com.t1.yd.tm.api.ICommandService;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.controller.CommandController;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    public void run(String[] args){
        commandController.showWelcome();
        processArguments(args);

        Scanner scanner = new Scanner(System.in);

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(scanner.nextLine());
        }
    }

    private void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private void processArgument(String arg) {
        switch (arg) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
    }

    private void processCommand(String arg) {
        switch (arg) {
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.INFO:
                commandController.showInfo();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
