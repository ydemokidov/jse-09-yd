package com.t1.yd.tm.api;

import com.t1.yd.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
