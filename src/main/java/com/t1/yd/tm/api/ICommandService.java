package com.t1.yd.tm.api;

import com.t1.yd.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
